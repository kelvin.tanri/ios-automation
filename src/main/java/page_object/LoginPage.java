package page_object;

import io.appium.java_client.AppiumBy;
import org.openqa.selenium.By;

import static utils.Utils.env;

public class LoginPage extends BasePageObjects {
    public void entryPointLoginElement() {
        assertIsDisplay("LANDING_PAGE_VIEW");
    }

    public void tapNextButtonAllowTracking() {
        tap("BUTTON_SKIP_TRACKING");
    }

    public void isEntryPointLoginButton() {
        assertIsDisplay("LANDING_PAGE_VIEW");
    }

    public void tapEntryPointLoginButton(){
        tap("LANDING_PAGE_VIEW");
    }

    public void inputFieldUsername(String username) {
        typeOn("FIELD_USERNAME_LOGIN", env(username));
    }

    public void isInvalidUsernamePassword() {
        assertIsDisplay("ERROR_INVALID_USERNAME_PASSWORD");
    }

    public void isBlankUsernamePassword() {
        assertIsDisplay("ERROR_BLANK_USERNAME_PASSWORD");
    }

    public void inputFieldPassword(String password) {
        typeOn("FIELD_PASSWORD_LOGIN", env(password));
    }

    public void tapLoginButton() {
        tap("BUTTON_LOGIN");
    }

    public void isBlankUsername() {
        assertIsDisplay("ERROR_BLANK_USERNAME");
    }
    public void isBlankPassword() {
        assertIsDisplay("ERROR_BLANK_PASSWORD");
    }
    public void tapLoginSkip() {
        tap("BUTTON_SKIP_BIOMETRIC");
    }
    public void tapLoginSkipAvatar() {
        tap("BUTTON_SKIP_AVATAR");
    }
    public void isWatchlistPage() {
        assertIsDisplay("LANDING_PAGE_WATCHLIST");
    }
}
