package steps_definition;

import io.cucumber.java8.En;
import page_object.LoginPage;

public class LoginSteps implements En {

    LoginPage loginPage = new LoginPage();
    public LoginSteps() {
        Given("^user click next button track app$", () -> loginPage.tapNextButtonAllowTracking()
        );
        When("^user see entry point login$", () -> loginPage.isEntryPointLoginButton()
        );
        And("^user click entry point login$", () -> loginPage.tapEntryPointLoginButton()
        );
        And("^user input username as \"([^\"]*)\"$", (String username) -> loginPage.inputFieldUsername(username)
        );
        And("^user input password as \"([^\"]*)\"$", (String password) -> loginPage.inputFieldPassword(password)
        );
        And("^user click button login$", () -> loginPage.tapLoginButton()
        );
        Then("^user see error message invalid username and password$", () -> loginPage.isInvalidUsernamePassword()
        );
        And("^user input wrong password as \"([^\"]*)\"$", (String password) -> loginPage.inputFieldPassword(password)
        );
        Then("^user see error message blank username and password$", () -> loginPage.isBlankUsernamePassword()
        );
        Then("^user see error message blank username$", () -> loginPage.isBlankUsername()
        );
        Then("^user see error message blank password$", () -> loginPage.isBlankPassword()
        );
        And("^user click skip$", () -> loginPage.tapLoginSkip()
        );
        Then("^user see the watchlist$", () -> loginPage.isWatchlistPage()
        );
        And("^user click skip avatar$", () -> loginPage.tapLoginSkipAvatar()
        );

    }
}
