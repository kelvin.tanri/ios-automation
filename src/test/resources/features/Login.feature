@iOS @LoginFeature
  Feature: [iOS] Login Feature

    @iOS @InvalidPassword
    Scenario: [iOS] User Login with Invalid Password
      Given user click next button track app
      When user see entry point login
      And user click entry point login
      And user input username as "USERNAME_PROD"
      And user input wrong password as "PASSWORD_INVALID"
      And user click button login
      Then user see error message invalid username and password

    @iOS @BlankUsernamePassword
    Scenario: [iOS] User Login with Blank Username and Password
      Given user click next button track app
      When user see entry point login
      And user click entry point login
      And user input username as "USERNAME_BLANK"
      And user input password as "PASSWORD_BLANK"
      And user click button login
      Then user see error message blank username and password

    @iOS @InvalidUsername
    Scenario: [iOS] User Login with Invalid Username
      Given user click next button track app
      When user see entry point login
      And user click entry point login
      And user input username as "USERNAME_INVALID"
      And user input password as "PASSWORD_PROD"
      And user click button login
      Then user see error message invalid username and password

    @iOS @SuccessLogin
    Scenario: [iOS] User Login with Success Login
      Given user click next button track app
      When user see entry point login
      And user click entry point login
      And user input username as "USERNAME_PROD"
      And user input password as "PASSWORD_PROD"
      And user click button login
      And user click skip
      And user click skip avatar
      Then user see the watchlist

    @iOS @BlankUsername
    Scenario: [iOS] User Login with Blank Username
      Given user click next button track app
      When user see entry point login
      And user click entry point login
      And user input username as "USERNAME_BLANK"
      And user input password as "PASSWORD_PROD"
      And user click button login
      Then user see error message blank username

    @iOS @BlankPassword
    Scenario: [iOS] User Login with Blank Password
      Given user click next button track app
      When user see entry point login
      And user click entry point login
      And user input username as "USERNAME_PROD"
      And user input password as "PASSWORD_BLANK"
      And user click button login
      Then user see error message blank password
