# ios-automation



## Dependencies

1. **Appium**: Appium is an open-source automation tool primarily used for testing mobile applications. It allows testers to automate mobile app testing across various platforms like Android and iOS using the same API.
2. **Cucumber**: Cucumber is a popular open-source testing tool used for behavior-driven development (BDD). It allows software teams to write test scenarios in a human-readable format called Gherkin, which uses keywords like Given, When, and Then to describe behavior.
3. **Java**: Java is a widely-used programming language known for its platform independence and versatility. Java provide external libraries or modules that are necessary for automating tasks or testing processes in Java-based automation frameworks.
4. **Selenium**: Selenium is an open-source automation testing tool primarily used for automating web applications. It provides a suite of tools and libraries for automating web browsers across different platforms.

## Setup Instruction

1. **Install Homebrew**: Open terminal, paste the following command and press enter:
    ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
2. **Install Java JDK 17**: Open terminal, paste the following command and press enter:
    ```
    brew install openjdk@17
   ```
3. **Install Node**: Open terminal, paste the following command and press enter:
    ```
    brew install node
    ```
4. **Install Appium 2**: Open terminal, paste the following command and press enter:
    ```
    npm install -g appium@2
    ```

## User Guide

1. **Run Appium**: Open terminal, paste the following command and press enter:
    ```
   Appium
    ```
2. **Open IDE**
3. **Run Login.feature file**